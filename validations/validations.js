const Joi = require('joi');

const passwordFormat = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/);
const contactNumberFormat = new RegExp(/^(\+\d{1,3}[- ]?)?\d{10}$/)

const loginSchema = Joi.object({

    email_address: Joi.string().email().required().error(errors => {
        errors.forEach(err => {
            switch (err.code) {
                case "string.empty":
                    err.message = 'Email field cannot be left empty'
                    break
                case "string.email":
                    err.message = 'Please input a valid email address'
                    break
            }
        })
        return errors
    }),
    password: Joi.string().pattern(passwordFormat).min(8).max(20).required().error(errors => {
        errors.forEach(err => {
            switch (err.code) {
                case "string.pattern.base":
                    err.message = 'The password should contain atleast 1 lower case character, 1 upper case character, 1 special character and should have a length between 8 to 20.'
                    break
                case "string.min":
                    err.message = 'Password must be atleast 8 characters long'
                    break
                case "string.max":
                    err.message = 'Password must be atmost 20 characters long'
                    break
            }
        })
        return errors
    })
});

const registrationSchema = Joi.object({

    user_name: Joi.string().alphanum().min(5).max(40).required().error(errors => {
        errors.forEach(err => {
            switch (err.code) {
                case "string.min":
                    err.message = 'Username must be atleast 5 characters long'
                    break
                case "string.max":
                    err.message = 'Username must be atmost 40 characters long'
                    break
            }
        })
        return errors
    }),

    // contact_number: [
    //     Joi.number().required(),
    //     Joi.string().pattern(contactNumberFormat).required().error(errors => {
    //         errors.forEach(err => {
    //             switch (err.code) {
    //                 case "string.empty":
    //                     err.message = 'Please insert contact number'
    //                     break
    //                 case "string.pattern.base":
    //                     err.message = 'Please enter a valid contact number'
    //                     break
    //             }
    //         })
    //         return errors
    //     })
    // ],

    // address: Joi.string().required().min(5),

    email_address: Joi.string().email().required().error(errors => {
        errors.forEach(err => {
            switch (err.code) {
                case "string.empty":
                    err.message = 'Email field cannot be left empty'
                    break
                case "string.email":
                    err.message = 'Please input a valid email address'
                    break
            }
        })
        return errors
    }),
    password: Joi.string().pattern(passwordFormat).required().error(errors => {
        errors.forEach(err => {
            switch (err.code) {
                case "string.pattern.base":
                    err.message = 'The password should contain atleast 1 lower case character, 1 upper case character, 1 special character and should have a length between 8 to 20.'
                    break
                case "string.min":
                    err.message = 'Password must be atleast 8 characters long'
                    break
                case "string.max":
                    err.message = 'Password must be atmost 20 characters long'
                    break
            }
        })
        return errors
    })
});

module.exports = { loginSchema, registrationSchema }