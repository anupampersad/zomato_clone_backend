const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const config = require('../config')


router.get('/user/authenticate', async (req, res) => {

    let token = req.get("authorization");

    if (token) {

        token = token.split(' ')[1]

        jwt.verify(token, config.SECRET_KEY, (error, user) => {
            if (error) {
                res.json({ message: "Invalid Token",error:error })
            }
            else {

                res.json({ mesaage:"User Validated" ,userDetails: user })
            }
        });
    }
    else {
        res.json({
            message: "Access Denied. Please Login!!!"
        })
    }
})

module.exports = router;