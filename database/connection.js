const { Sequelize } = require('sequelize');
const config = require('../config');

// const sequelize = new Sequelize(process.env.DATABASE, process.env.USER, process.env.PASSWORD, {
//     host: process.env.HOST,
//     dialect: 'mysql'
// });

const sequelize = new Sequelize(config.URI);

const connectDB = async () => {

    try {
        await sequelize.authenticate();
        console.log("CONNECTION OPEN");
    }
    catch (error) {
        console.log("DB CONNECTION FAILED");
    }
}

sequelize.sync({
    force: false,
    logging: false
})

module.exports = connectDB;

global.sequelize = sequelize;