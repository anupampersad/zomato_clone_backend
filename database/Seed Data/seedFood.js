const { food } = require("../../models/models");

async function seedFood() {

    const data = [

        {
            food_id: 10000,
            food_name: 'Whopper',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 131
        },
        {
            food_id: 10001,
            food_name: 'Whopper with Cheese',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 108
        },
        {
            food_id: 10002,
            food_name: 'Double Whopper',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 178
        },
        {
            food_id: 10003,
            food_name: 'Double Whopper with Cheese',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 118
        },
        {
            food_id: 10004,
            food_name: 'Triple Whopper',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 155
        },
        {
            food_id: 10005,
            food_name: 'Texas Whopper',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 189
        },
        {
            food_id: 10006,
            food_name: 'Texas Double Whopper Sandwich',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 187
        },
        {
            food_id: 10007,
            food_name: 'Whopper Jr.',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 181
        },
        {
            food_id: 10008,
            food_name: 'Whopper Jr. with Cheese',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 103
        },
        {
            food_id: 10009,
            food_name: 'Hamburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 167
        },
        {
            food_id: 10010,
            food_name: 'Cheeseburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 185
        },
        {
            food_id: 10011,
            food_name: 'Double Hamburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 165
        },
        {
            food_id: 10012,
            food_name: 'Double Cheeseburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 125
        },
        {
            food_id: 10013,
            food_name: 'BK Bacon Burger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 105
        },
        {
            food_id: 10014,
            food_name: 'Bacon Cheeseburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 199
        },
        {
            food_id: 10015,
            food_name: 'Double Bacon Cheeseburger',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 153
        },
        {
            food_id: 10016,
            food_name: 'BK Single Stacker',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 107
        },
        {
            food_id: 10017,
            food_name: 'BK Double Stacker',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 197
        },
        {
            food_id: 10018,
            food_name: 'BK Triple Stacker',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 159
        },
        {
            food_id: 10019,
            food_name: 'BK Quad Stacker',
            restaurant_id: 1000,
            cuisine: 'Burgers',
            price: 190
        },



        {
            food_id: 10020,
            food_name: 'Tendergrill Chicken',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 214
        },
        {
            food_id: 10021,
            food_name: 'Tendercrisp Chicken',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 227
        },
        {
            food_id: 10022,
            food_name: 'Original Chicken',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 264
        },
        {
            food_id: 10023,
            food_name: 'Chicken Crisp Classic',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 225
        },
        {
            food_id: 10024,
            food_name: 'Chicken Crisp Spicy',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 243
        },
        {
            food_id: 10025,
            food_name: 'Home-style Chicken Strips',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 225
        },
        {
            food_id: 10026,
            food_name: 'Chicken Nuggets',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 227
        },
        {
            food_id: 10027,
            food_name: 'Tacos',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 208
        },
        {
            food_id: 10028,
            food_name: 'Country Pork Sandwich',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 281
        },
        {
            food_id: 10029,
            food_name: 'Premium Alaskan Fish Sandwich',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 253
        },
        {
            food_id: 10030,
            food_name: 'BK Veggie Burger',
            restaurant_id: 1000,
            cuisine: 'Chicken, Fish, and Veggie',
            price: 211
        },



        {
            food_id: 10031,
            food_name: 'Quaker Oatmeal Maple and Brown Sugar',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 214
        },
        {
            food_id: 10032,
            food_name: 'Quaker Oatmeal Original',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 221
        },
        {
            food_id: 10033,
            food_name: 'Sausage Breakfast Burrito',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 235
        },
        {
            food_id: 10034,
            food_name: 'Southwestern Breakfast Burrito',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 294
        },
        {
            food_id: 10035,
            food_name: 'BK Breakfast Muffin Sandwich',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 255
        },
        {
            food_id: 10036,
            food_name: "Croissan'wich",
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 291
        },
        {
            food_id: 10037,
            food_name: "Double Croissan'wich",
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 216
        },
        {
            food_id: 10038,
            food_name: 'Biscuit Sandwich',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 284
        },
        {
            food_id: 10039,
            food_name: 'Cinnabon Roll',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 278
        },
        {
            food_id: 10040,
            food_name: 'Hash Browns',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 204
        },
        {
            food_id: 10041,
            food_name: 'French Toast Sticks',
            restaurant_id: 1000,
            cuisine: 'Breakfast',
            price: 290
        },



        {
            food_id: 10042,
            food_name: 'Garden Fresh Salad Chicken Caesar with Tendergrill',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 287
        },
        {
            food_id: 10043,
            food_name: 'Garden Fresh Salad Chicken Caesar with Tendercrisp',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 211
        },
        {
            food_id: 10044,
            food_name: 'Garden Fresh Salad Chicken BLT with Tendergrill',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 255
        },
        {
            food_id: 10045,
            food_name: 'Garden Fresh Salad Chicken BLT with Tendercrisp',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 277
        },
        {
            food_id: 10046,
            food_name: 'Garden Fresh Salad Chicken Apple & Cranberry with Tendergrill',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 253
        },
        {
            food_id: 10047,
            food_name: 'Garden Fresh Salad Chicken Apple & Cranberry with Tendercrisp',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 215
        },
        {
            food_id: 10048,
            food_name: 'Side Garden Fresh Salad and Ranch Dressing',
            restaurant_id: 1000,
            cuisine: 'BK Garden Fresh Salads',
            price: 251
        },



        {
            food_id: 10049,
            food_name: 'Ranch Crispy Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 239
        },
        {
            food_id: 10050,
            food_name: 'Ranch Grilled Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 235
        },
        {
            food_id: 10051,
            food_name: 'Honey Mustard Crispy Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 256
        },
        {
            food_id: 10052,
            food_name: 'Honey Mustard Grilled Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 286
        },
        {
            food_id: 10053,
            food_name: 'Ceasar Crispy Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 247
        },
        {
            food_id: 10054,
            food_name: 'Ceasar Grilled Chicken Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 230
        },
        {
            food_id: 10055,
            food_name: 'Crispy Chicken Apple and Cranberry Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 270
        },
        {
            food_id: 10056,
            food_name: 'Grilled Chicken Apple and Cranberry Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 214
        },
        {
            food_id: 10057,
            food_name: 'Crispy BLT Salad Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 241
        },
        {
            food_id: 10058,
            food_name: 'Grilled BLT Salad Wrap',
            restaurant_id: 1000,
            cuisine: 'Wraps',
            price: 213
        },



        {
            food_id: 10059,
            food_name: 'Dutch Apple Pie',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 283
        },
        {
            food_id: 10060,
            food_name: "Hershey's Sundae Pie",
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 296
        },
        {
            food_id: 10061,
            food_name: 'Soft Serve Cone',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 296
        },
        {
            food_id: 10062,
            food_name: 'Soft Serve Cup',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 237
        },
        {
            food_id: 10063,
            food_name: 'Caramel Sundae',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 261
        },
        {
            food_id: 10064,
            food_name: 'Chocolate Fudge Sundae',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 249
        },
        {
            food_id: 10065,
            food_name: 'Strawberry Sundae',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 286
        },
        {
            food_id: 10066,
            food_name: 'Oreo Sundae',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 266
        },
        {
            food_id: 10067,
            food_name: "Mini M&M's Sundae",
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 214
        },
        {
            food_id: 10068,
            food_name: 'Warm Oreo Brownie Sundae',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 289
        },
        {
            food_id: 10069,
            food_name: 'Chocolate Chip Cookies',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 249
        },
        {
            food_id: 10070,
            food_name: 'White Chocolate Macadamia Nut Cookies',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 248
        },
        {
            food_id: 10071,
            food_name: 'Oatmeal Raisin Cookies',
            restaurant_id: 1000,
            cuisine: 'Desserts',
            price: 259
        },



        {
            food_id: 10072,
            food_name: 'Strawberry Banana Smoothie',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 201
        },
        {
            food_id: 10073,
            food_name: 'Tropical Mango Smoothie',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 237
        },
        {
            food_id: 10074,
            food_name: 'Vanilla Milk Shake',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 211
        },
        {
            food_id: 10075,
            food_name: 'Chocolate Milk Shake',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 242
        },
        {
            food_id: 10076,
            food_name: 'Strawberry Milk Shake',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 241
        },
        {
            food_id: 10077,
            food_name: 'Mocha Frappé',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 209
        },
        {
            food_id: 10078,
            food_name: 'Caramel Frappé',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 292
        },
        {
            food_id: 10079,
            food_name: 'Whipped topping coffee shot',
            restaurant_id: 1000,
            cuisine: 'Beverages',
            price: 259
        }

    ]

    food.bulkCreate(data)
        .then(() => console.log('Food data seeded successfully'))
}

module.exports = seedFood