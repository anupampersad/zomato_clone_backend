const { city } = require("../../models/models");

async function seedcity2() {

    const city_data =
    [
        { city_name: 'Alipur', city_id: 101,state_id: '1' },
        { city_name: 'Andaman Island', state_id: '1' },
        { city_name: 'Anderson Island', state_id: '1' },
        { city_name: 'Arainj-Laka-Punga', state_id: '1' },
        { city_name: 'Austinabad', state_id: '1' },
        { city_name: 'Bamboo Flat', state_id: '1' },
        { city_name: 'Barren Island', state_id: '1' },
        { city_name: 'Beadonabad', state_id: '1' },
        { city_name: 'Betapur', state_id: '1' },
        { city_name: 'Bindraban', state_id: '1' },
        { city_name: 'Bonington', state_id: '1' },
        { city_name: 'Brookesabad', state_id: '1' },
        { city_name: 'Cadell Point', state_id: '1' },
        { city_name: 'Calicut', state_id: '1' },
        { city_name: 'Chetamale', state_id: '1' },
        { city_name: 'Cinque Islands', state_id: '1' },
        { city_name: 'Defence Island', state_id: '1' },
        { city_name: 'Digilpur', state_id: '1' },
        { city_name: 'Dolyganj', state_id: '1' },
        { city_name: 'Flat Island', state_id: '1' },
        { city_name: 'Geinyale', state_id: '1' },
        { city_name: 'Great Coco Island', state_id: '1' },
        { city_name: 'Haddo', state_id: '1' },
        { city_name: 'Havelock Island', state_id: '1' },
        { city_name: 'Henry Lawrence Island', state_id: '1' },
        { city_name: 'Herbertabad', state_id: '1' },
        { city_name: 'Hobdaypur', state_id: '1' },
        { city_name: 'Ilichar', state_id: '1' },
        { city_name: 'Ingoie', state_id: '1' },
        { city_name: 'Inteview Island', state_id: '1' },
        { city_name: 'Jangli Ghat', state_id: '1' },
        { city_name: 'Jhon Lawrence Island', state_id: '1' },
        { city_name: 'Karen', state_id: '1' },
        { city_name: 'Kartara', state_id: '1' },
        { city_name: 'KYD Islannd', state_id: '1' },
        { city_name: 'Landfall Island', state_id: '1' },
        { city_name: 'Little Andmand', state_id: '1' },
        { city_name: 'Little Coco Island', state_id: '1' },
        { city_name: 'Long Island', state_id: '1' },
        { city_name: 'Maimyo', state_id: '1' },
        { city_name: 'Malappuram', state_id: '1' },
        { city_name: 'Manglutan', state_id: '1' },
        { city_name: 'Manpur', state_id: '1' },
        { city_name: 'Mitha Khari', state_id: '1' },
        { city_name: 'Neill Island', state_id: '1' },
        { city_name: 'Nicobar Island', state_id: '1' },
        { city_name: 'North Brother Island', state_id: '1' },
        { city_name: 'North Passage Island', state_id: '1' },
        { city_name: 'North Sentinel Island', state_id: '1' },
        { city_name: 'Nothen Reef Island', state_id: '1' },
        { city_name: 'Outram Island', state_id: '1' },
        { city_name: 'Pahlagaon', state_id: '1' },
        { city_name: 'Palalankwe', state_id: '1' },
        { city_name: 'Passage Island', state_id: '1' },
        { city_name: 'Phaiapong', state_id: '1' },
        { city_name: 'Phoenix Island', state_id: '1' },
        { city_name: 'Port Blair', state_id: '1' },
        { city_name: 'Preparis Island', state_id: '1' },
        { city_name: 'Protheroepur', state_id: '1' },
        { city_name: 'Rangachang', state_id: '1' },
        { city_name: 'Rongat', state_id: '1' },
        { city_name: 'Rutland Island', state_id: '1' },
        { city_name: 'Sabari', state_id: '1' },
        { city_name: 'Saddle Peak', state_id: '1' },
        { city_name: 'Shadipur', state_id: '1' },
        { city_name: 'Smith Island', state_id: '1' },
        { city_name: 'Sound Island', state_id: '1' },
        { city_name: 'South Sentinel Island', state_id: '1' },
        { city_name: 'Spike Island', state_id: '1' },
        { city_name: 'Tarmugli Island', state_id: '1' },
        { city_name: 'Taylerabad', state_id: '1' },
        { city_name: 'Titaije', state_id: '1' },
        { city_name: 'Toibalawe', state_id: '1' },
        { city_name: 'Tusonabad', state_id: '1' },
        { city_name: 'West Island', state_id: '1' },
        { city_name: 'Wimberleyganj', state_id: '1' },
        { city_name: 'Yadita', state_id: '1' },

        { city_name: 'Adilabad', state_id: '2' },
        { city_name: 'Anantapur', state_id: '2' },
        { city_name: 'Chittoor', state_id: '2' },
        { city_name: 'Cuddapah', state_id: '2' },
        { city_name: 'East Godavari', state_id: '2' },
        { city_name: 'Guntur', state_id: '2' },
        { city_name: 'Hyderabad', state_id: '2' },
        { city_name: 'Karimnagar', state_id: '2' },
        { city_name: 'Khammam', state_id: '2' },
        { city_name: 'Krishna', state_id: '2' },
        { city_name: 'Kurnool', state_id: '2' },
        { city_name: 'Mahabubnagar', state_id: '2' },
        { city_name: 'Medak', state_id: '2' },
        { city_name: 'Nalgonda', state_id: '2' },
        { city_name: 'Nellore', state_id: '2' },
        { city_name: 'Nizamabad', state_id: '2' },
        { city_name: 'Prakasam', state_id: '2' },
        { city_name: 'Rangareddy', state_id: '2' },
        { city_name: 'Srikakulam', state_id: '2' },
        { city_name: 'Visakhapatnam', state_id: '2' },
        { city_name: 'Vizianagaram', state_id: '2' },
        { city_name: 'Warangal', state_id: '2' },
        { city_name: 'West Godavari', state_id: '2' },

        { city_name: 'Anjaw', state_id: '3' },
        { city_name: 'Changlang', state_id: '3' },
        { city_name: 'Dibang Valley', state_id: '3' },
        { city_name: 'East Kameng', state_id: '3' },
        { city_name: 'East Siang', state_id: '3' },
        { city_name: 'Itanagar', state_id: '3' },
        { city_name: 'Kurung Kumey', state_id: '3' },
        { city_name: 'Lohit', state_id: '3' },
        { city_name: 'Lower Dibang Valley', state_id: '3' },
        { city_name: 'Lower Subansiri', state_id: '3' },
        { city_name: 'Papum Pare', state_id: '3' },
        { city_name: 'Tawang', state_id: '3' },
        { city_name: 'Tirap', state_id: '3' },
        { city_name: 'Upper Siang', state_id: '3' },
        { city_name: 'Upper Subansiri', state_id: '3' },
        { city_name: 'West Kameng', state_id: '3' },
        { city_name: 'West Siang', state_id: '3' },

        { city_name: 'Barpeta', state_id: '4' },
        { city_name: 'Bongaigaon', state_id: '4' },
        { city_name: 'Cachar', state_id: '4' },
        { city_name: 'Darrang', state_id: '4' },
        { city_name: 'Dhemaji', state_id: '4' },
        { city_name: 'Dhubri', state_id: '4' },
        { city_name: 'Dibrugarh', state_id: '4' },
        { city_name: 'Goalpara', state_id: '4' },
        { city_name: 'Golaghat', state_id: '4' },
        { city_name: 'Guwahati', state_id: '4' },
        { city_name: 'Hailakandi', state_id: '4' },
        { city_name: 'Jorhat', state_id: '4' },
        { city_name: 'Kamrup', state_id: '4' },
        { city_name: 'Karbi Anglong', state_id: '4' },
        { city_name: 'Karimganj', state_id: '4' },
        { city_name: 'Kokrajhar', state_id: '4' },
        { city_name: 'Lakhimpur', state_id: '4' },
        { city_name: 'Marigaon', state_id: '4' },
        { city_name: 'Nagaon', state_id: '4' },
        { city_name: 'Nalbari', state_id: '4' },
        { city_name: 'North Cachar Hills', state_id: '4' },
        { city_name: 'Silchar', state_id: '4' },
        { city_name: 'Sivasagar', state_id: '4' },
        { city_name: 'Sonitpur', state_id: '4' },
        { city_name: 'Tinsukia', state_id: '4' },
        { city_name: 'Udalguri', state_id: '4' },

        { city_name: 'Araria', state_id: '5' },
        { city_name: 'Aurangabad', state_id: '5' },
        { city_name: 'Banka', state_id: '5' },
        { city_name: 'Begusarai', state_id: '5' },
        { city_name: 'Bhagalpur', state_id: '5' },
        { city_name: 'Bhojpur', state_id: '5' },
        { city_name: 'Buxar', state_id: '5' },
        { city_name: 'Darbhanga', state_id: '5' },
        { city_name: 'East Champaran', state_id: '5' },
        { city_name: 'Gaya', state_id: '5' },
        { city_name: 'Gopalganj', state_id: '5' },
        { city_name: 'Jamshedpur', state_id: '5' },
        { city_name: 'Jamui', state_id: '5' },
        { city_name: 'Jehanabad', state_id: '5' },
        { city_name: 'Kaimu(Bhabua}', state_id: '5' },
        { city_name: 'Katihar', state_id: '5' },
        { city_name: 'Khagaria', state_id: '5' },
        { city_name: 'Kishanganj', state_id: '5' },
        { city_name: 'Lakhisarai', state_id: '5' },
        { city_name: 'Madhepura', state_id: '5' },
        { city_name: 'Madhubani', state_id: '5' },
        { city_name: 'Munger', state_id: '5' },
        { city_name: 'Muzaffarpur', state_id: '5' },
        { city_name: 'Nalanda', state_id: '5' },
        { city_name: 'Nawada', state_id: '5' },
        { city_name: 'Patna', state_id: '5' },
        { city_name: 'Purnia', state_id: '5' },
        { city_name: 'Rohtas', state_id: '5' },
        { city_name: 'Saharsa', state_id: '5' },
        { city_name: 'Samastipur', state_id: '5' },
        { city_name: 'Saran', state_id: '5' },
        { city_name: 'Sheikhpura', state_id: '5' },
        { city_name: 'Sheohar', state_id: '5' },
        { city_name: 'Sitamarhi', state_id: '5' },
        { city_name: 'Siwan', state_id: '5' },
        { city_name: 'Supaul', state_id: '5' },
        { city_name: 'Vaishali', state_id: '5' },
        { city_name: 'West Champaran', state_id: '5' },

        { city_name: 'Chandigarh', state_id: '6' },
        { city_name: 'Mani Marja', state_id: '6' },

        { city_name: 'Bastar', state_id: '7' },
        { city_name: 'Bhilai', state_id: '7' },
        { city_name: 'Bijapur', state_id: '7' },
        { city_name: 'Bilaspur', state_id: '7' },
        { city_name: 'Dhamtari', state_id: '7' },
        { city_name: 'Durg', state_id: '7' },
        { city_name: 'Janjgir-Champa', state_id: '7' },
        { city_name: 'Jashpur', state_id: '7' },
        { city_name: 'Kabirdham-Kawardha', state_id: '7' },
        { city_name: 'Korba', state_id: '7' },
        { city_name: 'Korea', state_id: '7' },
        { city_name: 'Mahasamund', state_id: '7' },
        { city_name: 'Narayanpur', state_id: '7' },
        { city_name: 'Norh Bastar-Kanker', state_id: '7' },
        { city_name: 'Raigarh', state_id: '7' },
        { city_name: 'Raipur', state_id: '7' },
        { city_name: 'Rajnandgaon', state_id: '7' },
        { city_name: 'South Bastar-Dantewada', state_id: '7' },
        { city_name: 'Surguja', state_id: '7' },

        { city_name: 'Amal', state_id: '8' },
        { city_name: 'Amli', state_id: '8' },
        { city_name: 'Bedpa', state_id: '8' },
        { city_name: 'Chikhli', state_id: '8' },
        { city_name: 'Dadra & Nagar Haveli', state_id: '8' },
        { city_name: 'Dahikhed', state_id: '8' },
        { city_name: 'Dolara', state_id: '8' },
        { city_name: 'Galonda', state_id: '8' },
        { city_name: 'Kanadi', state_id: '8' },
        { city_name: 'Karchond', state_id: '8' },
        { city_name: 'Khadoli', state_id: '8' },
        { city_name: 'Kharadpada', state_id: '8' },
        { city_name: 'Kherabari', state_id: '8' },
        { city_name: 'Kherdi', state_id: '8' },
        { city_name: 'Kothar', state_id: '8' },
        { city_name: 'Luari', state_id: '8' },
        { city_name: 'Mashat', state_id: '8' },
        { city_name: 'Rakholi', state_id: '8' },
        { city_name: 'Rudana', state_id: '8' },
        { city_name: 'Saili', state_id: '8' },
        { city_name: 'Sili', state_id: '8' },
        { city_name: 'Silvassa', state_id: '8' },
        { city_name: 'Sindavni', state_id: '8' },
        { city_name: 'Udva', state_id: '8' },
        { city_name: 'Umbarkoi', state_id: '8' },
        { city_name: 'Vansda', state_id: '8' },
        { city_name: 'Vasona', state_id: '8' },
        { city_name: 'Velugam', state_id: '8' },

        { city_name: 'Brancavare', state_id: '9' },
        { city_name: 'Dagasi', state_id: '9' },
        { city_name: 'Daman', state_id: '9' },
        { city_name: 'Diu', state_id: '9' },
        { city_name: 'Magarvara', state_id: '9' },
        { city_name: 'Nagwa', state_id: '9' },
        { city_name: 'Pariali', state_id: '9' },
        { city_name: 'Passo Covo', state_id: '9' },

        { city_name: 'Central Delhi', state_id: '10' },
        { city_name: 'East Delhi', state_id: '10' },
        { city_name: 'New Delhi', state_id: '10' },
        { city_name: 'North Delhi', state_id: '10' },
        { city_name: 'North East Delhi', state_id: '10' },
        { city_name: 'North West Delhi', state_id: '10' },
        { city_name: 'Old Delhi', state_id: '10' },
        { city_name: 'South Delhi', state_id: '10' },
        { city_name: 'South West Delhi', state_id: '10' },
        { city_name: 'West Delhi', state_id: '10' },

        { city_name: 'Canacona', state_id: '11' },
        { city_name: 'Candolim', state_id: '11' },
        { city_name: 'Chinchinim', state_id: '11' },
        { city_name: 'Cortalim', state_id: '11' },
        { city_name: 'Goa', state_id: '11' },
        { city_name: 'Jua', state_id: '11' },
        { city_name: 'Madgaon', state_id: '11' },
        { city_name: 'Mahem', state_id: '11' },
        { city_name: 'Mapuca', state_id: '11' },
        { city_name: 'Marmagao', state_id: '11' },
        { city_name: 'Panji', state_id: '11' },
        { city_name: 'Ponda', state_id: '11' },
        { city_name: 'Sanvordem', state_id: '11' },
        { city_name: 'Terekhol', state_id: '11' },

        { city_name: 'Ahmedabad', state_id: '12' },
        { city_name: 'Amreli', state_id: '12' },
        { city_name: 'Anand', state_id: '12' },
        { city_name: 'Banaskantha', state_id: '12' },
        { city_name: 'Baroda', state_id: '12' },
        { city_name: 'Bharuch', state_id: '12' },
        { city_name: 'Bhavnagar', state_id: '12' },
        { city_name: 'Dahod', state_id: '12' },
        { city_name: 'Dang', state_id: '12' },
        { city_name: 'Dwarka', state_id: '12' },
        { city_name: 'Gandhinagar', state_id: '12' },
        { city_name: 'Jamnagar', state_id: '12' },
        { city_name: 'Junagadh', state_id: '12' },
        { city_name: 'Kheda', state_id: '12' },
        { city_name: 'Kutch', state_id: '12' },
        { city_name: 'Mehsana', state_id: '12' },
        { city_name: 'Nadiad', state_id: '12' },
        { city_name: 'Narmada', state_id: '12' },
        { city_name: 'Navsari', state_id: '12' },
        { city_name: 'Panchmahals', state_id: '12' },
        { city_name: 'Patan', state_id: '12' },
        { city_name: 'Porbandar', state_id: '12' },
        { city_name: 'Rajkot', state_id: '12' },
        { city_name: 'Sabarkantha', state_id: '12' },
        { city_name: 'Surat', state_id: '12' },
        { city_name: 'Surendranagar', state_id: '12' },
        { city_name: 'Vadodara', state_id: '12' },
        { city_name: 'Valsad', state_id: '12' },
        { city_name: 'Vapi', state_id: '12' },

        { city_name: 'Ambala', state_id: '13' },
        { city_name: 'Bhiwani', state_id: '13' },
        { city_name: 'Faridabad', state_id: '13' },
        { city_name: 'Fatehabad', state_id: '13' },
        { city_name: 'Gurgaon', state_id: '13' },
        { city_name: 'Hisar', state_id: '13' },
        { city_name: 'Jhajjar', state_id: '13' },
        { city_name: 'Jind', state_id: '13' },
        { city_name: 'Kaithal', state_id: '13' },
        { city_name: 'Karnal', state_id: '13' },
        { city_name: 'Kurukshetra', state_id: '13' },
        { city_name: 'Mahendragarh', state_id: '13' },
        { city_name: 'Mewat', state_id: '13' },
        { city_name: 'Panchkula', state_id: '13' },
        { city_name: 'Panipat', state_id: '13' },
        { city_name: 'Rewari', state_id: '13' },
        { city_name: 'Rohtak', state_id: '13' },
        { city_name: 'Sirsa', state_id: '13' },
        { city_name: 'Sonipat', state_id: '13' },
        { city_name: 'Yamunanagar', state_id: '13' },

        { city_name: 'Bilaspur', state_id: '14' },
        { city_name: 'Chamba', state_id: '14' },
        { city_name: 'Dalhousie', state_id: '14' },
        { city_name: 'Hamirpur', state_id: '14' },
        { city_name: 'Kangra', state_id: '14' },
        { city_name: 'Kinnaur', state_id: '14' },
        { city_name: 'Kullu', state_id: '14' },
        { city_name: 'Lahaul & Spiti', state_id: '14' },
        { city_name: 'Mandi', state_id: '14' },
        { city_name: 'Shimla', state_id: '14' },
        { city_name: 'Sirmaur', state_id: '14' },
        { city_name: 'Solan', state_id: '14' },
        { city_name: 'Una', state_id: '14' },

        { city_name: 'Anantnag', state_id: '15' },
        { city_name: 'Baramulla', state_id: '15' },
        { city_name: 'Budgam', state_id: '15' },
        { city_name: 'Doda', state_id: '15' },
        { city_name: 'Jammu', state_id: '15' },
        { city_name: 'Kargil', state_id: '15' },
        { city_name: 'Kathua', state_id: '15' },
        { city_name: 'Kupwara', state_id: '15' },
        { city_name: 'Leh', state_id: '15' },
        { city_name: 'Poonch', state_id: '15' },
        { city_name: 'Pulwama', state_id: '15' },
        { city_name: 'Rajauri', state_id: '15' },
        { city_name: 'Srinagar', state_id: '15' },
        { city_name: 'Udhampur', state_id: '15' },

        { city_name: 'Bokaro', state_id: '16' },
        { city_name: 'Chatra', state_id: '16' },
        { city_name: 'Deoghar', state_id: '16' },
        { city_name: 'Dhanbad', state_id: '16' },
        { city_name: 'Dumka', state_id: '16' },
        { city_name: 'East Singhbhum', state_id: '16' },
        { city_name: 'Garhwa', state_id: '16' },
        { city_name: 'Giridih', state_id: '16' },
        { city_name: 'Godda', state_id: '16' },
        { city_name: 'Gumla', state_id: '16' },
        { city_name: 'Hazaribag', state_id: '16' },
        { city_name: 'Jamtara', state_id: '16' },
        { city_name: 'Koderma', state_id: '16' },
        { city_name: 'Latehar', state_id: '16' },
        { city_name: 'Lohardaga', state_id: '16' },
        { city_name: 'Pakur', state_id: '16' },
        { city_name: 'Palamu', state_id: '16' },
        { city_name: 'Ranchi', state_id: '16' },
        { city_name: 'Sahibganj', state_id: '16' },
        { city_name: 'Seraikela', state_id: '16' },
        { city_name: 'Simdega', state_id: '16' },
        { city_name: 'West Singhbhum', state_id: '16' },

        { city_name: 'Bagalkot', state_id: '17' },
        { city_name: 'Bangalore', state_id: '17' },
        { city_name: 'Bangalore Rural', state_id: '17' },
        { city_name: 'Belgaum', state_id: '17' },
        { city_name: 'Bellary', state_id: '17' },
        { city_name: 'Bhatkal', state_id: '17' },
        { city_name: 'Bidar', state_id: '17' },
        { city_name: 'Bijapur', state_id: '17' },
        { city_name: 'Chamrajnagar', state_id: '17' },
        { city_name: 'Chickmagalur', state_id: '17' },
        { city_name: 'Chikballapur', state_id: '17' },
        { city_name: 'Chitradurga', state_id: '17' },
        { city_name: 'Dakshina Kannada', state_id: '17' },
        { city_name: 'Davanagere', state_id: '17' },
        { city_name: 'Dharwad', state_id: '17' },
        { city_name: 'Gadag', state_id: '17' },
        { city_name: 'Gulbarga', state_id: '17' },
        { city_name: 'Hampi', state_id: '17' },
        { city_name: 'Hassan', state_id: '17' },
        { city_name: 'Haveri', state_id: '17' },
        { city_name: 'Hospet', state_id: '17' },
        { city_name: 'Karwar', state_id: '17' },
        { city_name: 'Kodagu', state_id: '17' },
        { city_name: 'Kolar', state_id: '17' },
        { city_name: 'Koppal', state_id: '17' },
        { city_name: 'Madikeri', state_id: '17' },
        { city_name: 'Mandya', state_id: '17' },
        { city_name: 'Mangalore', state_id: '17' },
        { city_name: 'Manipal', state_id: '17' },
        { city_name: 'Mysore', state_id: '17' },
        { city_name: 'Raichur', state_id: '17' },
        { city_name: 'Shimoga', state_id: '17' },
        { city_name: 'Sirsi', state_id: '17' },
        { city_name: 'Sringeri', state_id: '17' },
        { city_name: 'Srirangapatna', state_id: '17' },
        { city_name: 'Tumkur', state_id: '17' },
        { city_name: 'Udupi', state_id: '17' },
        { city_name: 'Uttara Kannada', state_id: '17' },

        { city_name: 'Alappuzha', state_id: '18' },
        { city_name: 'Alleppey', state_id: '18' },
        { city_name: 'Alwaye', state_id: '18' },
        { city_name: 'Ernakulam', state_id: '18' },
        { city_name: 'Idukki', state_id: '18' },
        { city_name: 'Kannur', state_id: '18' },
        { city_name: 'Kasargod', state_id: '18' },
        { city_name: 'Kochi', state_id: '18' },
        { city_name: 'Kollam', state_id: '18' },
        { city_name: 'Kottayam', state_id: '18' },
        { city_name: 'Kovalam', state_id: '18' },
        { city_name: 'Kozhikode', state_id: '18' },
        { city_name: 'Malappuram', state_id: '18' },
        { city_name: 'Palakkad', state_id: '18' },
        { city_name: 'Pathanamthitta', state_id: '18' },
        { city_name: 'Perumbavoor', state_id: '18' },
        { city_name: 'Thiruvananthapuram', state_id: '18' },
        { city_name: 'Thrissur', state_id: '18' },
        { city_name: 'Trichur', state_id: '18' },
        { city_name: 'Trivandrum', state_id: '18' },
        { city_name: 'Wayanad', state_id: '18' },

        { city_name: 'Agatti Island', state_id: '19' },
        { city_name: 'Bingaram Island', state_id: '19' },
        { city_name: 'Bitra Island', state_id: '19' },
        { city_name: 'Chetlat Island', state_id: '19' },
        { city_name: 'Kadmat Island', state_id: '19' },
        { city_name: 'Kalpeni Island', state_id: '19' },
        { city_name: 'Kavaratti Island', state_id: '19' },
        { city_name: 'Kiltan Island', state_id: '19' },
        { city_name: 'Lakshadweep Sea', state_id: '19' },
        { city_name: 'Minicoy Island', state_id: '19' },
        { city_name: 'North Island', state_id: '19' },
        { city_name: 'South Island', state_id: '19' },

        { city_name: 'Anuppur', state_id: '20' },
        { city_name: 'Ashoknagar', state_id: '20' },
        { city_name: 'Balaghat', state_id: '20' },
        { city_name: 'Barwani', state_id: '20' },
        { city_name: 'Betul', state_id: '20' },
        { city_name: 'Bhind', state_id: '20' },
        { city_name: 'Bhopal', state_id: '20' },
        { city_name: 'Burhanpur', state_id: '20' },
        { city_name: 'Chhatarpur', state_id: '20' },
        { city_name: 'Chhindwara', state_id: '20' },
        { city_name: 'Damoh', state_id: '20' },
        { city_name: 'Datia', state_id: '20' },
        { city_name: 'Dewas', state_id: '20' },
        { city_name: 'Dhar', state_id: '20' },
        { city_name: 'Dindori', state_id: '20' },
        { city_name: 'Guna', state_id: '20' },
        { city_name: 'Gwalior', state_id: '20' },
        { city_name: 'Harda', state_id: '20' },
        { city_name: 'Hoshangabad', state_id: '20' },
        { city_name: 'Indore', state_id: '20' },
        { city_name: 'Jabalpur', state_id: '20' },
        { city_name: 'Jagdalpur', state_id: '20' },
        { city_name: 'Jhabua', state_id: '20' },
        { city_name: 'Katni', state_id: '20' },
        { city_name: 'Khandwa', state_id: '20' },
        { city_name: 'Khargone', state_id: '20' },
        { city_name: 'Mandla', state_id: '20' },
        { city_name: 'Mandsaur', state_id: '20' },
        { city_name: 'Morena', state_id: '20' },
        { city_name: 'Narsinghpur', state_id: '20' },
        { city_name: 'Neemuch', state_id: '20' },
        { city_name: 'Panna', state_id: '20' },
        { city_name: 'Raisen', state_id: '20' },
        { city_name: 'Rajgarh', state_id: '20' },
        { city_name: 'Ratlam', state_id: '20' },
        { city_name: 'Rewa', state_id: '20' },
        { city_name: 'Sagar', state_id: '20' },
        { city_name: 'Satna', state_id: '20' },
        { city_name: 'Sehore', state_id: '20' },
        { city_name: 'Seoni', state_id: '20' },
        { city_name: 'Shahdol', state_id: '20' },
        { city_name: 'Shajapur', state_id: '20' },
        { city_name: 'Sheopur', state_id: '20' },
        { city_name: 'Shivpuri', state_id: '20' },
        { city_name: 'Sidhi', state_id: '20' },
        { city_name: 'Tikamgarh', state_id: '20' },
        { city_name: 'Ujjain', state_id: '20' },
        { city_name: 'Umaria', state_id: '20' },
        { city_name: 'Vidisha', state_id: '20' },

        { city_name: 'Ahmednagar', state_id: '21' },
        { city_name: 'Akola', state_id: '21' },
        { city_name: 'Amravati', state_id: '21' },
        { city_name: 'Aurangabad', state_id: '21' },
        { city_name: 'Beed', state_id: '21' },
        { city_name: 'Bhandara', state_id: '21' },
        { city_name: 'Buldhana', state_id: '21' },
        { city_name: 'Chandrapur', state_id: '21' },
        { city_name: 'Dhule', state_id: '21' },
        { city_name: 'Gadchiroli', state_id: '21' },
        { city_name: 'Gondia', state_id: '21' },
        { city_name: 'Hingoli', state_id: '21' },
        { city_name: 'Jalgaon', state_id: '21' },
        { city_name: 'Jalna', state_id: '21' },
        { city_name: 'Kolhapur', state_id: '21' },
        { city_name: 'Latur', state_id: '21' },
        { city_name: 'Mahabaleshwar', state_id: '21' },
        { city_name: 'Mumbai', state_id: '21' },
        { city_name: 'Mumbai City', state_id: '21' },
        { city_name: 'Mumbai Suburban', state_id: '21' },
        { city_name: 'Nagpur', state_id: '21' },
        { city_name: 'Nanded', state_id: '21' },
        { city_name: 'Nandurbar', state_id: '21' },
        { city_name: 'Nashik', state_id: '21' },
        { city_name: 'Osmanabad', state_id: '21' },
        { city_name: 'Parbhani', state_id: '21' },
        { city_name: 'Pune', state_id: '21' },
        { city_name: 'Raigad', state_id: '21' },
        { city_name: 'Ratnagiri', state_id: '21' },
        { city_name: 'Sangli', state_id: '21' },
        { city_name: 'Satara', state_id: '21' },
        { city_name: 'Sholapur', state_id: '21' },
        { city_name: 'Sindhudurg', state_id: '21' },
        { city_name: 'Thane', state_id: '21' },
        { city_name: 'Wardha', state_id: '21' },
        { city_name: 'Washim', state_id: '21' },
        { city_name: 'Yavatmal', state_id: '21' },

        { city_name: 'Bishnupur', state_id: '22' },
        { city_name: 'Chandel', state_id: '22' },
        { city_name: 'Churachandpur', state_id: '22' },
        { city_name: 'Imphal East', state_id: '22' },
        { city_name: 'Imphal West', state_id: '22' },
        { city_name: 'Senapati', state_id: '22' },
        { city_name: 'Tamenglong', state_id: '22' },
        { city_name: 'Thoubal', state_id: '22' },
        { city_name: 'Ukhrul', state_id: '22' },

        { city_name: 'East Garo Hills', state_id: '23' },
        { city_name: 'East Khasi Hills', state_id: '23' },
        { city_name: 'Jaintia Hills', state_id: '23' },
        { city_name: 'Ri Bhoi', state_id: '23' },
        { city_name: 'Shillong', state_id: '23' },
        { city_name: 'South Garo Hills', state_id: '23' },
        { city_name: 'West Garo Hills', state_id: '23' },
        { city_name: 'West Khasi Hills', state_id: '23' },

        // -- Mizoram
        { city_name: 'Aizawl', state_id: '24' },
        { city_name: 'Champhai', state_id: '24' },
        { city_name: 'Kolasib', state_id: '24' },
        { city_name: 'Lawngtlai', state_id: '24' },
        { city_name: 'Lunglei', state_id: '24' },
        { city_name: 'Mamit', state_id: '24' },
        { city_name: 'Saiha', state_id: '24' },
        { city_name: 'Serchhip', state_id: '24' },

        { city_name: 'Dimapur', state_id: '25' },
        { city_name: 'Kohima', state_id: '25' },
        { city_name: 'Mokokchung', state_id: '25' },
        { city_name: 'Mon', state_id: '25' },
        { city_name: 'Phek', state_id: '25' },
        { city_name: 'Tuensang', state_id: '25' },
        { city_name: 'Wokha', state_id: '25' },
        { city_name: 'Zunheboto', state_id: '25' },

        { city_name: 'Angul', state_id: '26' },
        { city_name: 'Balangir', state_id: '26' },
        { city_name: 'Balasore', state_id: '26' },
        { city_name: 'Baleswar', state_id: '26' },
        { city_name: 'Bargarh', state_id: '26' },
        { city_name: 'Berhampur', state_id: '26' },
        { city_name: 'Bhadrak', state_id: '26' },
        { city_name: 'Bhubaneswar', state_id: '26' },
        { city_name: 'Boudh', state_id: '26' },
        { city_name: 'Cuttack', state_id: '26' },
        { city_name: 'Deogarh', state_id: '26' },
        { city_name: 'Dhenkanal', state_id: '26' },
        { city_name: 'Gajapati', state_id: '26' },
        { city_name: 'Ganjam', state_id: '26' },
        { city_name: 'Jagatsinghapur', state_id: '26' },
        { city_name: 'Jajpur', state_id: '26' },
        { city_name: 'Jharsuguda', state_id: '26' },
        { city_name: 'Kalahandi', state_id: '26' },
        { city_name: 'Kandhamal', state_id: '26' },
        { city_name: 'Kendrapara', state_id: '26' },
        { city_name: 'Kendujhar', state_id: '26' },
        { city_name: 'Khordha', state_id: '26' },
        { city_name: 'Koraput', state_id: '26' },
        { city_name: 'Malkangiri', state_id: '26' },
        { city_name: 'Mayurbhanj', state_id: '26' },
        { city_name: 'Nabarangapur', state_id: '26' },
        { city_name: 'Nayagarh', state_id: '26' },
        { city_name: 'Nuapada', state_id: '26' },
        { city_name: 'Puri', state_id: '26' },
        { city_name: 'Rayagada', state_id: '26' },
        { city_name: 'Rourkela', state_id: '26' },
        { city_name: 'Sambalpur', state_id: '26' },
        { city_name: 'Subarnapur', state_id: '26' },
        { city_name: 'Sundergarh', state_id: '26' },

        { city_name: 'Bahur', state_id: '27' },
        { city_name: 'Karaikal', state_id: '27' },
        { city_name: 'Mahe', state_id: '27' },
        { city_name: 'Pondicherry', state_id: '27' },
        { city_name: 'Purnankuppam', state_id: '27' },
        { city_name: 'Valudavur', state_id: '27' },
        { city_name: 'Villianur', state_id: '27' },
        { city_name: 'Yanam', state_id: '27' },

        { city_name: 'Amritsar', state_id: '28' },
        { city_name: 'Barnala', state_id: '28' },
        { city_name: 'Bathinda', state_id: '28' },
        { city_name: 'Faridkot', state_id: '28' },
        { city_name: 'Fatehgarh Sahib', state_id: '28' },
        { city_name: 'Ferozepur', state_id: '28' },
        { city_name: 'Gurdaspur', state_id: '28' },
        { city_name: 'Hoshiarpur', state_id: '28' },
        { city_name: 'Jalandhar', state_id: '28' },
        { city_name: 'Kapurthala', state_id: '28' },
        { city_name: 'Ludhiana', state_id: '28' },
        { city_name: 'Mansa', state_id: '28' },
        { city_name: 'Moga', state_id: '28' },
        { city_name: 'Muktsar', state_id: '28' },
        { city_name: 'Nawanshahr', state_id: '28' },
        { city_name: 'Pathankot', state_id: '28' },
        { city_name: 'Patiala', state_id: '28' },
        { city_name: 'Rupnagar', state_id: '28' },
        { city_name: 'Sangrur', state_id: '28' },
        { city_name: 'SAS Nagar', state_id: '28' },
        { city_name: 'Tarn Taran', state_id: '28' },

        { city_name: 'Ajmer', state_id: '29' },
        { city_name: 'Alwar', state_id: '29' },
        { city_name: 'Banswara', state_id: '29' },
        { city_name: 'Baran', state_id: '29' },
        { city_name: 'Barmer', state_id: '29' },
        { city_name: 'Bharatpur', state_id: '29' },
        { city_name: 'Bhilwara', state_id: '29' },
        { city_name: 'Bikaner', state_id: '29' },
        { city_name: 'Bundi', state_id: '29' },
        { city_name: 'Chittorgarh', state_id: '29' },
        { city_name: 'Churu', state_id: '29' },
        { city_name: 'Dausa', state_id: '29' },
        { city_name: 'Dholpur', state_id: '29' },
        { city_name: 'Dungarpur', state_id: '29' },
        { city_name: 'Hanumangarh', state_id: '29' },
        { city_name: 'Jaipur', state_id: '29' },
        { city_name: 'Jaisalmer', state_id: '29' },
        { city_name: 'Jalore', state_id: '29' },
        { city_name: 'Jhalawar', state_id: '29' },
        { city_name: 'Jhunjhunu', state_id: '29' },
        { city_name: 'Jodhpur', state_id: '29' },
        { city_name: 'Karauli', state_id: '29' },
        { city_name: 'Kota', state_id: '29' },
        { city_name: 'Nagaur', state_id: '29' },
        { city_name: 'Pali', state_id: '29' },
        { city_name: 'Pilani', state_id: '29' },
        { city_name: 'Rajsamand', state_id: '29' },
        { city_name: 'Sawai Madhopur', state_id: '29' },
        { city_name: 'Sikar', state_id: '29' },
        { city_name: 'Sirohi', state_id: '29' },
        { city_name: 'Sri Ganganagar', state_id: '29' },
        { city_name: 'Tonk', state_id: '29' },
        { city_name: 'Udaipur', state_id: '29' },

        { city_name: 'Barmiak', state_id: '30' },
        { city_name: 'Be', state_id: '30' },
        { city_name: 'Bhurtuk', state_id: '30' },
        { city_name: 'Chhubakha', state_id: '30' },
        { city_name: 'Chidam', state_id: '30' },
        { city_name: 'Chubha', state_id: '30' },
        { city_name: 'Chumikteng', state_id: '30' },
        { city_name: 'Dentam', state_id: '30' },
        { city_name: 'Dikchu', state_id: '30' },
        { city_name: 'Dzongri', state_id: '30' },
        { city_name: 'Gangtok', state_id: '30' },
        { city_name: 'Gauzing', state_id: '30' },
        { city_name: 'Gyalshing', state_id: '30' },
        { city_name: 'Hema', state_id: '30' },
        { city_name: 'Kerung', state_id: '30' },
        { city_name: 'Lachen', state_id: '30' },
        { city_name: 'Lachung', state_id: '30' },
        { city_name: 'Lema', state_id: '30' },
        { city_name: 'Lingtam', state_id: '30' },
        { city_name: 'Lungthu', state_id: '30' },
        { city_name: 'Mangan', state_id: '30' },
        { city_name: 'Namchi', state_id: '30' },
        { city_name: 'Namthang', state_id: '30' },
        { city_name: 'Nanga', state_id: '30' },
        { city_name: 'Nantang', state_id: '30' },
        { city_name: 'Naya Bazar', state_id: '30' },
        { city_name: 'Padamachen', state_id: '30' },
        { city_name: 'Pakhyong', state_id: '30' },
        { city_name: 'Pemayangtse', state_id: '30' },
        { city_name: 'Phensang', state_id: '30' },
        { city_name: 'Rangli', state_id: '30' },
        { city_name: 'Rinchingpong', state_id: '30' },
        { city_name: 'Sakyong', state_id: '30' },
        { city_name: 'Samdong', state_id: '30' },
        { city_name: 'Singtam', state_id: '30' },
        { city_name: 'Siniolchu', state_id: '30' },
        { city_name: 'Sombari', state_id: '30' },
        { city_name: 'Soreng', state_id: '30' },
        { city_name: 'Sosing', state_id: '30' },
        { city_name: 'Tekhug', state_id: '30' },
        { city_name: 'Temi', state_id: '30' },
        { city_name: 'Tsetang', state_id: '30' },
        { city_name: 'Tsomgo', state_id: '30' },
        { city_name: 'Tumlong', state_id: '30' },
        { city_name: 'Yangang', state_id: '30' },
        { city_name: 'Yumtang', state_id: '30' },

        { city_name: 'Chennai', state_id: '31' },
        { city_name: 'Chidambaram', state_id: '31' },
        { city_name: 'Chingleput', state_id: '31' },
        { city_name: 'Coimbatore', state_id: '31' },
        { city_name: 'Courtallam', state_id: '31' },
        { city_name: 'Cuddalore', state_id: '31' },
        { city_name: 'Dharmapuri', state_id: '31' },
        { city_name: 'Dindigul', state_id: '31' },
        { city_name: 'Erode', state_id: '31' },
        { city_name: 'Hosur', state_id: '31' },
        { city_name: 'Kanchipuram', state_id: '31' },
        { city_name: 'Kanyakumari', state_id: '31' },
        { city_name: 'Karaikudi', state_id: '31' },
        { city_name: 'Karur', state_id: '31' },
        { city_name: 'Kodaikanal', state_id: '31' },
        { city_name: 'Kovilpatti', state_id: '31' },
        { city_name: 'Krishnagiri', state_id: '31' },
        { city_name: 'Kumbakonam', state_id: '31' },
        { city_name: 'Madurai', state_id: '31' },
        { city_name: 'Mayiladuthurai', state_id: '31' },
        { city_name: 'Nagapattinam', state_id: '31' },
        { city_name: 'Nagarcoil', state_id: '31' },
        { city_name: 'Namakkal', state_id: '31' },
        { city_name: 'Neyveli', state_id: '31' },
        { city_name: 'Nilgiris', state_id: '31' },
        { city_name: 'Ooty', state_id: '31' },
        { city_name: 'Palani', state_id: '31' },
        { city_name: 'Perambalur', state_id: '31' },
        { city_name: 'Pollachi', state_id: '31' },
        { city_name: 'Pudukkottai', state_id: '31' },
        { city_name: 'Rajapalayam', state_id: '31' },
        { city_name: 'Ramanathapuram', state_id: '31' },
        { city_name: 'Salem', state_id: '31' },
        { city_name: 'Sivaganga', state_id: '31' },
        { city_name: 'Sivakasi', state_id: '31' },
        { city_name: 'Thanjavur', state_id: '31' },
        { city_name: 'Theni', state_id: '31' },
        { city_name: 'Thoothukudi', state_id: '31' },
        { city_name: 'Tiruchirappalli', state_id: '31' },
        { city_name: 'Tirunelveli', state_id: '31' },
        { city_name: 'Tirupur', state_id: '31' },
        { city_name: 'Tiruvallur', state_id: '31' },
        { city_name: 'Tiruvannamalai', state_id: '31' },
        { city_name: 'Tiruvarur', state_id: '31' },
        { city_name: 'Trichy', state_id: '31' },
        { city_name: 'Tuticorin', state_id: '31' },
        { city_name: 'Vellore', state_id: '31' },
        { city_name: 'Villupuram', state_id: '31' },
        { city_name: 'Virudhunagar', state_id: '31' },
        { city_name: 'Yercaud', state_id: '31' },

        { city_name: 'Agartala', state_id: '32' },
        { city_name: 'Ambasa', state_id: '32' },
        { city_name: 'Bampurbari', state_id: '32' },
        { city_name: 'Belonia', state_id: '32' },
        { city_name: 'Dhalai', state_id: '32' },
        { city_name: 'Dharam Nagar', state_id: '32' },
        { city_name: 'Kailashahar', state_id: '32' },
        { city_name: 'Kamal Krishnabari', state_id: '32' },
        { city_name: 'Khopaiyapara', state_id: '32' },
        { city_name: 'Khowai', state_id: '32' },
        { city_name: 'Phuldungsei', state_id: '32' },
        { city_name: 'Radha Kishore Pur', state_id: '32' },
        { city_name: 'Tripura', state_id: '32' }
    ]

    city.bulkCreate(city_data)
        .then(() => console.log('City data seeded successfully'))
}

module.exports = seedcity2