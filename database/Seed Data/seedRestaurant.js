const { restaurant } = require("../../models/models");

async function seedRestaurant() {

    const data = [
        { restaurant_id: 1000, restaurant_name: 'Burger King', city_id: 242, contact_number: 9999999999 },
        { restaurant_id: 1001, restaurant_name: 'Haldirams', city_id: 243, contact_number: 9999999999 },
        { restaurant_id: 1002, restaurant_name: 'BTW', city_id: 244, contact_number: 9999999999 },
        { restaurant_id: 1003, restaurant_name: 'KFC', city_id: 241, contact_number: 9999999999 },
        { restaurant_id: 1004, restaurant_name: 'Burger Singh', city_id: 246, contact_number: 9999999999 },
        { restaurant_id: 1005, restaurant_name: 'Dominos', city_id: 240, contact_number: 9999999999 },
        { restaurant_id: 1006, restaurant_name: 'Dunkin Donuts', city_id: 239, contact_number: 9999999999 },
        { restaurant_id: 1007, restaurant_name: 'Faasos', city_id: 246, contact_number: 9999999999 },
        { restaurant_id: 1008, restaurant_name: 'Biryani Blues', city_id: 244, contact_number: 9999999999 },
        { restaurant_id: 1009, restaurant_name: 'Bikanerwala', city_id: 239, contact_number: 9999999999 },
        { restaurant_id: 1010, restaurant_name: 'Krispy Kreme', city_id: 245, contact_number: 9999999999 },
        { restaurant_id: 1011, restaurant_name: 'McDonalds', city_id: 243, contact_number: 9999999999 },
        { restaurant_id: 1012, restaurant_name: 'Moti Mahal Delux', city_id: 242, contact_number: 9999999999 },
        { restaurant_id: 1013, restaurant_name: 'Om Sweets & Snacks', city_id: 239, contact_number: 9999999999 },
        { restaurant_id: 1014, restaurant_name: 'Paradise Biryani', city_id: 242, contact_number: 9999999999 },
        { restaurant_id: 1015, restaurant_name: 'Pizza Hut', city_id: 244, contact_number: 9999999999 },
        { restaurant_id: 1016, restaurant_name: 'Sagar Ratna', city_id: 242, contact_number: 9999999999 },
        { restaurant_id: 1017, restaurant_name: 'Subway', city_id: 240, contact_number: 9999999999 },
        { restaurant_id: 1018, restaurant_name: 'WOW! Momo', city_id: 247, contact_number: 9999999999 }
    ]

    restaurant.bulkCreate(data)
        .then(() => console.log('Restaurant data seeded successfully'))
}

module.exports = seedRestaurant
