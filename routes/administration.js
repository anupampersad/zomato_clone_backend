const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const config = require('../config')

const auth = require('../middlewares/auth');

const { user } = require('../models/models')

const bcrypt = require('bcrypt');
const saltRounds = config.SALT_ROUNDS;

const jwt = require('jsonwebtoken')

const { loginSchema, registrationSchema } = require('../validations/validations')

// Add a new user
router.post('/register', async (req, res) => {

    try {

        await registrationSchema.validateAsync(req.body);

        const password = req.body.password;
        const email_address = req.body.email_address;
        // const contact_number = req.body.contact_number;

        const isEmailPresent = await user.findOne({
            where: {
                email_address: email_address
            },
            attributes: ['email_address']
        });

        // const isContactNumberPresent = await user.findOne({
        //     where: {
        //         contact_number: contact_number
        //     },
        //     attributes: ['contact_number']
        // });

        // Checking if user with given email id or moble number exists or not
        if (isEmailPresent) {
            return res.status(404).json({ "message": "User already exists with this email id" })
        }
        // if (isContactNumberPresent) {
        //     return res.status(404).json({ "message": "User already exists with this contact number" })
        // }

        const encryptedPassword = await bcrypt.hash(password, saltRounds);

        const addUser = { ...req.body, password: encryptedPassword };

        await user.create(addUser);

        res.json({ message: "User Added Successfully" });
    }
    catch (error) {
        console.log(error)
        return res.status(400).json({ "message": "Invalid Request"})
    }
})

router.post('/login', async (req, res) => {

    try {

        await loginSchema.validateAsync(req.body);

        ({ email_address, password } = req.body);

        const userRequired = await user.findOne({
            where: {
                email_address: email_address
            },
            attributes: ['email_address', 'password','user_name','contact_number']
        });

        if (userRequired == null) {
            return res.status(404).json({ message: "No user registered with this email id" })
        }

        const match = await bcrypt.compare(password, userRequired.password);

        if (match) {

            userRequired.password = undefined;
            //This is because we do not want to send password in jsontoken

            // Once we sign using below step, in 'result : userRequired.dataValues'  we are passing all the data of the user
            // which is fetched from above using querry except password because that is made undefined
            // Instead of using jwt.sign({ result: userRequired.dataValues } ,
            // it will be better to sign with above method i.e. jwt.sign({ ... userRequired.dataValues }

            const jsonToken = jwt.sign({ ...userRequired.dataValues }, config.SECRET_KEY, {
                expiresIn: "1h"
            });

            return res.json({
                message: "Login Successful",
                token: jsonToken
            });
        }
        else {
            res.status(404).json({ "message": "User ID or Password are Incorrect" })
        }
    }
    catch (error) {
        console.log(error)
        return res.status(400).json({ "error" :error.details[0].message})
    }
})

module.exports = router;