const express = require('express');
const router = express.Router();
const { restaurant, food } = require('../models/models')

// Get all restaurants
router.get('/restaurants', async (req, res) => {

    try {
        const restaurants = await restaurant.findAll({
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            }
        });

        res.json(restaurants)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get details of a single restaurant by using restaurant id
router.get('/restaurant/:restaurantID', async (req, res) => {

    const restaurantID = req.params.restaurantID

    try {
        const restaurants = await restaurant.findByPk(restaurantID, {
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            }
        });

        res.json(restaurants)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

// Get all food items for a particular restaurant
router.get('/restaurant/:restaurantID/order', async (req, res) => {

    const restaurantID = req.params.restaurantID

    try {
        const restaurants = await restaurant.findByPk(restaurantID, {
            attributes: {
                exclude: ['createdAt', 'updatedAt']
            },
            include: [{ model: food, attributes: ['food_name', 'price', 'cuisine'] }]
        });

        res.json(restaurants)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

module.exports = router;