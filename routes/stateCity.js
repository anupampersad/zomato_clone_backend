const express = require('express');
const router = express.Router();
const { state,city } = require('../models/models')

router.get('/states', async (req, res) => {

    try {
        const states = await state.findAll({
            attributes: ['state_name','state_id']
        });

        res.json(states)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

router.get('/cities', async (req, res) => {

    try {
        const cities = await city.findAll({
            attributes: ['city_name','city_id','state_id']
        });

        res.json(cities)
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})


module.exports = router;