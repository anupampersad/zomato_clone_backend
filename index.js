// Setting up express app
const express = require('express');
const app = express()
const port = process.env.PORT || 8080

require('dotenv').config()
const config = require('./config');

// Connecting to the database
const connectDB = require('./database/connection');
connectDB()

// Body and json parsing middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }));

// CORS
const cors = require('cors');
// app.use((req, res, next) => {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "*");
//     if(req.method === "OPTIONS"){
//         res.header("Access-Control-Allow-Methods","PUT,POST,PATCH,DELETE,GET");
//         return res.status(200).json({})
//     }
//     next();
// });
app.use(cors())

// Requring Models
const { user, restaurant, employee, food, state, city, order_details, payment } = require('./models/models');

// Requiring data seeding functions
const seedState = require('./database/Seed Data/seedState');
const seedcity2 = require('./database/Seed Data/seedcity2');
const seedRestaurant = require('./database/Seed Data/seedRestaurant');
const seedFood = require('./database/Seed Data/seedFood');

// seedState()
// seedcity2()
// seedRestaurant()
// seedFood()

const userRoutes = require('./routes/administration');
const stateCity = require("./routes/stateCity");
const restaurantFood = require("./routes/restaurantFood");
const auth = require('./middlewares/auth')

app.use(userRoutes);
app.use(stateCity);
app.use(restaurantFood)
app.use(auth)

app.get('/', (req, res) => {
    res.json({ message: "Heroku app setup" })
})

app.listen(port, () => {
    console.log(`Server running on port ${port}`)
});