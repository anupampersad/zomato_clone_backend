const { Sequelize, DataTypes } = require('sequelize');

// user
const user = sequelize.define('user', {
    user_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_name: {
        type: DataTypes.STRING,
        allowNull: true
    },
    contact_number: {
        type: DataTypes.STRING,
        allowNull: true,
        unique: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: true
    },
    email_address: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    }, password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    isPRO: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue:false
    }
});

// restaurant
const restaurant = sequelize.define('restaurant', {
    restaurant_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    restaurant_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    contact_number: {
        type: DataTypes.STRING,
        allowNull: false
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet gravida orci. Suspendisse sit amet felis nec ipsum porttitor laoreet.'
    }
});

// employee
const employee = sequelize.define('employee', {
    employee_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    employee_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    contact_number: {
        type: DataTypes.STRING,
        allowNull: false
    },
    rating: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

});

// food
const food = sequelize.define('food', {
    food_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    food_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cuisine: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT,
        allowNull: false
    }
});

// state
const state = sequelize.define('state', {
    state_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    state_name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

// city
const city = sequelize.define('city', {
    city_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        initialAutoIncrement: '1000'
    },
    city_name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

// discount
const discount = sequelize.define('discount', {
    discount_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    percentage_discount: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
});

// order details
const order_details = sequelize.define('order_details', {
    order_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status_code: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    order_time: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },
    delivery_time: {
        type: DataTypes.DATE,
        onUpdate: Sequelize.NOW
    }
});

// payment details
const payment = sequelize.define('payment', {
    payment_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status_code: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    mode: {
        type: DataTypes.STRING,
        allowNull: false
    },
    transaction_id: {
        type: DataTypes.INTEGER
    }
});

// RELATIONSHIPS

// One restaurant has many food. Food belongs to one restaurant
restaurant.hasMany(food, { foreignKey: { name: 'restaurant_id', allowNull: false } });
food.belongsTo(restaurant, { foreignKey: { name: 'restaurant_id', allowNull: false } });

//  One state has many city. City belongs to one state
state.hasMany(city, { foreignKey: { name: 'state_id', allowNull: false } });
city.belongsTo(state, { foreignKey: { name: 'state_id', allowNull: false } });

// One city has many restaurants. Restaurant belongs to one city.
city.hasMany(restaurant, { foreignKey: { name: 'city_id', allowNull: false } });
restaurant.belongsTo(city, { foreignKey: { name: 'city_id', allowNull: false } });

// One city has manny employee. An employee serves in a city.
city.hasMany(employee, { foreignKey: { name: 'employee_id', allowNull: false } });
employee.belongsTo(city, { foreignKey: { name: 'employee_id', allowNull: false } });

// One food item can have many discount codes. Discount code can be linked to many food items.
food.belongsToMany(discount, { foreignKey: 'food_id', through: "food_discount" });
discount.belongsToMany(food, { foreignKey: 'discount_id', through: "food_discount" });

// Order details must have user, restaurant, employee
user.hasMany(order_details, { foreignKey: { name: 'user_id', allowNull: false } });
restaurant.hasMany(order_details, { foreignKey: { name: 'restaurant_id', allowNull: false } });
employee.hasMany(order_details, { foreignKey: { name: 'employee_id', allowNull: false } });
order_details.belongsTo(user, { foreignKey: { name: 'user_id', allowNull: false } });
order_details.belongsTo(restaurant, { foreignKey: { name: 'restaurant_id', allowNull: false } });
order_details.belongsTo(employee, { foreignKey: { name: 'employee_id', allowNull: false } });

// A single order detail can have many food items. A food item can be present in many orders.
order_details.belongsToMany(food, { foreignKey: 'order_id', through: "order_food" });
food.belongsToMany(order_details, { foreignKey: 'food_id', through: "order_food" });

// Every payment is linked to a order_detail
payment.belongsTo(order_details, { foreignKey: { name: 'order_id', allowNull: false } });

module.exports = { user, restaurant, employee, food, state, city, order_details, payment };